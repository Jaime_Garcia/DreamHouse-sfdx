trigger BatchApexErrorTrigger on BatchApexErrorEvent ( after insert ) {
    List<BatchLeadConvertErrors__c> batchLeadConvertErrorsList = new List<BatchLeadConvertErrors__c> ();
    for (BatchApexErrorEvent batchApexError : Trigger.New) {
        BatchLeadConvertErrors__c batchLeadConvertErrors = new BatchLeadConvertErrors__c(
            AsyncApexJobId__c = batchApexError.AsyncApexJobId,
            Records__c = batchApexError.JobScope,
            StackTrace__c = batchApexError.StackTrace
        );
        batchLeadConvertErrorsList.add(batchLeadConvertErrors);
    }
    insert batchLeadConvertErrorsList;
}
